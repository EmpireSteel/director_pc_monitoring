import os
from datetime import datetime
import sys
import json


path = os.path.dirname(os.path.realpath(__file__))
log_file = os.path.join(path, 'logs', str(datetime.now().date()) + '.log')
windows = []
with open(log_file) as f:
    for line in f.readlines():
        windows.append({"{#WINDOW}": line.split(';')[0].strip()})
data = {'data': windows}
print json.dumps(data, ensure_ascii=False)