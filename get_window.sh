#!/bin/bash

SCRIPTDIR=`dirname $0`
source $SCRIPTDIR/settings

idwindow="$(xdpyinfo -display $DSP | grep -oP '(?<=focus:\ \ window\ )(.+?)(?=,)')"
xwininfo -display $DSP -id $idwindow -children
