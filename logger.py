# coding=utf-8
import os
from datetime import datetime
import pwd
import sys
import stat


blacklist = ['pcmanfm', 'openbox', 'Openbox']
FILENAME = '.'.join([str(datetime.now().date()), 'log'])
USER_ID = str(pwd.getpwuid(os.getuid())[0])
BASE_DIR = os.path.dirname(os.path.realpath(__file__))
LOGS_DIR = os.path.join(BASE_DIR, 'logs')
LOG_FILE = os.path.join(LOGS_DIR, FILENAME)
if not os.path.exists(LOGS_DIR):
    os.mkdir(LOGS_DIR)
    os.chmod(LOGS_DIR, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)

if not os.path.exists(LOG_FILE):
    f = open(LOG_FILE, 'w').close()
    os.chmod(LOG_FILE, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)

for log in os.listdir(LOGS_DIR):
    if log != FILENAME:
        try:
            os.remove(log)
        except:
            pass
        

windows = os.popen('/bin/bash ' + os.path.join(BASE_DIR, 'get_window.sh')).read()
match = False
window = windows.split('\n')[4].split('"')[1:-1]
window = "'".join(window)
if window == "":
    window = windows.split('\n')[1].split('"')[1:-1]
    window = "'".join(window)
    if window == '':
        window = 'no named window'
window = window.replace(' ', '_').replace("'", '')
if window not in blacklist:
    try:
        with open(LOG_FILE, 'r') as f:
            log = f.readlines()
        with open(LOG_FILE, 'w') as f:
            for i in log:
                i = i.split(';')
                if i[0] == USER_ID + '_' + window:
                    match = True
                    i[1] = str(int(i[1]) + 60) + '\n'
                f.write(';'.join(i))
            if not match:
                f.write(USER_ID + '_' + window + ';60\n')
        os.chmod(LOG_FILE, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
    except IOError:
        with open(LOG_FILE, 'w') as f:
            f.write(USER_ID + '_' + window + ';60\n')
        os.chmod(LOG_FILE, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
