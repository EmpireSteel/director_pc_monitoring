# coding=utf-8
from datetime import datetime
import sys
import os


def get_value(window, log):
    with open(log, 'r') as f:
        for i in f.readlines():
            line = i.split(';')
            if window ==  line[0]:
                return int(line[1])
    return 0


if __name__ == '__main__':
    path = os.path.dirname(sys.argv[0])
    window = sys.argv[1]
    log = path + '/logs/' + str(datetime.now().date()) + '.log'
    print get_value(window, log)
