#!/bin/bash

SCRIPTDIR=`dirname $0`
cd ${SCRIPTDIR}
source ./settings
mkdir -p ${SCRIPTDIR}/run/${USER}
CLOSDIR=${SCRIPTDIR}/run/${USER}/${CLOSER}
LOGDIR=${SCRIPTDIR}/run/${USER}/${LOGGER}
chmod 777 ${SCRIPTDIR}/run
chmod 777 ${SCRIPTDIR}/run/*

case "$1"
in
start)
${SCRIPTDIR}/runner.sh stop > /dev/null
if [[ "test -f ${CLOSDIR}" && "test -f ${LOGDIR}" ]]
then
        ${SCRIPTDIR}/closer.sh &
        echo $! > ${CLOSDIR}
        ${SCRIPTDIR}/crone.sh &
        echo $! > ${LOGDIR}
        echo "Big Brother started!"
fi
;;
stop)
if test -f ${CLOSDIR}
then
        read PID < ${CLOSDIR}
        kill -9 ${PID}
        rm -rf ${CLOSDIR}
fi
if test -f ${LOGDIR}
then
        read PID < ${LOGDIR}
        kill -9 ${PID}
        rm -rf ${LOGDIR}
fi
echo "Big Brother shutdown..."
;;
help)
echo "use ( start | stop )."
;;
esac
